package main

import (
	"fmt"
	"net"
	"os"

	"gitlab.com/ybot/my-eui64/eui"
)

func main() {
	mac, err := net.ParseMAC(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
	fmt.Print(eui.Eui64(mac))
}
