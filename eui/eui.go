package eui

import (
	"net"
)

func Eui64(mac net.HardwareAddr) net.IP {
	octets := []byte(mac)
	euibin := []byte{0xfe, 0x80, 0, 0, 0, 0, 0, 0, octets[0] ^ 2, octets[1], octets[2], 0xff, 0xfe, octets[3], octets[4], octets[5]}
	return net.IP(euibin)
}

func Eui48(ip net.IP) net.HardwareAddr {
	octets := []byte(ip)
	macbin := []byte{octets[8] ^ 2, octets[9], octets[10], octets[13], octets[14], octets[15]}
	return net.HardwareAddr(macbin)
}
