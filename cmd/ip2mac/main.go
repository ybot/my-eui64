package main

import (
	"fmt"
	"net"
	"os"

	"gitlab.com/ybot/my-eui64/eui"
)

func main() {
	ip := net.ParseIP(os.Args[1])
	if ip == nil || ip.To4() != nil {
		//if ip == nil {
		fmt.Fprintf(os.Stderr, "error: %v is not a valid IPv6\n", os.Args[1])
		os.Exit(1)
	}
	fmt.Print(eui.Eui48(ip))
}
